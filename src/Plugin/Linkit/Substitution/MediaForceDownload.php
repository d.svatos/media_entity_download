<?php

namespace Drupal\media_entity_download\Plugin\Linkit\Substitution;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Template\Attribute;

/**
 * A substitution plugin for a direct download link to a file.
 *
 * @Substitution(
 *   id = "media_force_download",
 *   label = @Translation("Direct download URL for media item (Forcing Save as... browser dialog)"),
 * )
 */
class MediaForceDownload extends MediaDownload {

  /**
   * Get the URL associated with a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get a URL for.
   *
   * @return \Drupal\Core\GeneratedUrl
   *   A url to replace.
   */
  public function getUrl(EntityInterface $entity) {
    $url = new GeneratedUrl();

    $url_object = $this->getDownloadUrl($entity, TRUE);
    $url->setGeneratedUrl($url_object->toString());
    $url->addCacheableDependency($entity);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getElementAttributes(EntityInterface $entity) {
    $attributes = parent::getElementAttributes($entity);
    $attributes->setAttribute('download', NULL);
    return $attributes;
  }
}
