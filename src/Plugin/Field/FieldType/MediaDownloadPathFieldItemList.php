<?php

namespace Drupal\media_entity_download\Plugin\Field\FieldType;

use Drupal\path\Plugin\Field\FieldType\PathFieldItemList;

/**
 * Represents a configurable media entity download_path field.
 */
class MediaDownloadPathFieldItemList extends PathFieldItemList {

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    // Default the langcode to the current language if this is a new entity or
    // there is no alias for an existent entity.
    // @todo Set the langcode to not specified for untranslatable fields
    //   in https://www.drupal.org/node/2689459.
    $value = ['langcode' => $this->getLangcode()];

    $entity = $this->getEntity();
    if (!$entity->isNew()) {
      // @todo Support loading languge neutral aliases in
      //   https://www.drupal.org/node/2511968.
      $alias = \Drupal::service('path.alias_storage')->load([
        'source' => '/' . MediaDownloadPathItem::getMediaDownloadPath($entity),
        'langcode' => $this->getLangcode(),
      ]);

      if ($alias) {
        $value = $alias;
      }
    }

    $this->list[0] = $this->createItem(0, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    // Delete all aliases associated with this entity in the current language.
    $entity = $this->getEntity();
    $conditions = [
      'source' => '/' . MediaDownloadPathItem::getMediaDownloadPath($entity),
      'langcode' => $entity->language()->getId(),
    ];
    \Drupal::service('path.alias_storage')->delete($conditions);
  }

}
