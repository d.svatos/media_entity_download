(function ($, Drupal) {
  Drupal.behaviors.downloadPathDetailsSummaries = {
    attach: function attach(context) {
      $('details.media-download-path-form', context).drupalSetSummary(function (context) {
        let path = $('.js-form-item-media-download-path-0-alias input', context).val();
        return path ? Drupal.t('Alias: @alias', { '@alias': path }) : Drupal.t('No alias');
      });
    }
  };
})(jQuery, Drupal);
